# Indo e Vindo

Project to aggregate local bus schedules in an easy-to-search UI.

The crawlers were built with [Scrapy](http://scrapy.org), the web application
with [Flask](http://flask.pocoo.org) to run on Google App Engine.


## Running dev environment

1. Install the requirements to run the crawlers:

    `pip install -r crawlers/requirements.txt`

2. Run the crawlers using the script provided, that will
put the data into a directory inside the web application (`webapp/static/data`)

    `./scripts/run_crawler.py jotur canasvieiras`

3. At this step, you can run the tests written for the data used by the web application:

    `pip install pytest`
    `./scripts/deploy-webapp.sh --tests-only`

4. Download the dependencies that will be used by the web application into the `webapp/lib` directory:

    `pip install -r webapp/requirements.txt -t webapp/lib`

5. Now, to run the web application, you need first to install the
[Google App Engine Python SDK](https://developers.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python).
After installing it, you can run the web application using the command:

    `dev_appserver.py webapp/`


## Current architecture

As Scrapy spiders can't run on GAE, to avoid the infrastructure costs of
getting a VPS server just to serve them, I've decided to just use static
JSON files crawled before deploy.

The downside of doing that is, to update the data in the website, you need to
run the crawlers locally and then redeploy the webapp.
The scripts for doing that are under the `scripts/` directory.

