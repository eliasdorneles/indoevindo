#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import unittest
from model import BusLine, BusTimes, BusTimetable, Repo

class BusLineTest(unittest.TestCase):
    def test_get_key(self):
        busline = BusLine(name='Line Name', company='company',
                          ext_id='1', ext_link='http://www.google.com.br')
        self.assertEquals('company:1', busline.get_key())

class BusTimetableTest(unittest.TestCase):
    def setUp(self):
        times = [
            {"terminal": "Terminal", "company": "company", "bus_line": "262",
             "day_type": "Dias uteis", "times": ["05:33", "06:04"]},
            {"terminal": "Bairro", "company": "company", "bus_line": "262",
             "day_type": "Dias uteis", "times": ["05:33", "06:04"]},
            {"terminal": "Terminal", "company": "company", "bus_line": "262",
             "day_type": "Sabados", "times": ["05:33"]},
            {"terminal": "Terminal", "company": "company", "bus_line": "262",
             "day_type": "Domingos", "times": ["07:05"]},
        ]
        self.timetable = BusTimetable([BusTimes(**it) for it in times])

    def test_agg_by_day_type(self):
        # when:
        agg_by_day_type = self.timetable.by_day_type()
        # then:
        self.assertSetEqual(set(['Dias uteis', 'Sabados', 'Domingos']),
                            set(agg_by_day_type.keys()))
        self.assertSetEqual(set(['Terminal', 'Bairro']),
                            set(agg_by_day_type['Dias uteis'].keys()))

    def test_agg_by_terminal(self):
        # when:
        agg_by_terminal = self.timetable.by_terminal()
        # then:
        self.assertSetEqual(set(['Terminal', 'Bairro']),
                            set(agg_by_terminal.keys()))
        self.assertSetEqual(set(['Dias uteis', 'Domingos', 'Sabados']),
                            set(agg_by_terminal['Terminal'].keys()))


class RepoTest(unittest.TestCase):
    def setUp(self):
        self.repo = Repo()

    def test_get_companies(self):
        companies = self.repo.get_companies()
        self.assertIsNotNone(companies)
        self.assertTrue(len(companies) > 0)

    def test_get_buslines(self):
        # when:
        buslines = self.repo.get_buslines()
        # then:
        self.assertTrue(len(buslines) > 0, "No bus lines found")
        self.assertTrue(len([l for l in buslines if l.company == 'canasvieiras']),
                        "No bus line found for company canasvieiras")
    def test_get_busline_by_id(self):
        # setup:
        busline_id = 'canasvieiras:260'
        # when:
        busline = self.repo.get_busline_by_id(busline_id)
        # then:
        self.assertIsNotNone(busline)
        self.assertEquals('canasvieiras', busline.company)
        self.assertEquals('260', busline.ext_id)
        self.assertEquals('260 Cachoeira', busline.name)

    def test_get_timetable(self):
        # setup:
        busline_id = 'canasvieiras:260'
        busline = self.repo.get_busline_by_id(busline_id)
        # when:
        timetable = self.repo.get_timetable(busline)
        # then:
        self.assertIsNotNone(timetable)
        self.assertEquals(3, len(timetable))
        day_types = [t.day_type for t in timetable]
        self.assertIn('Domingos', day_types)
