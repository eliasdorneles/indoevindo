$(function(){
    var buslineSelector = $('.choose-busline');
    var timetableContainer = $('.timetable-container');
    var loadingMsg = $('.loading-msg');
    loadingMsg.hide(); // hide loading message initially

    buslineSelector.chosen();

    var updateTimetable = function() {
        loadingMsg.hide();
        var busline_id = window.location.hash.substr(1);
        $.get('/timetable/' + busline_id).done(function(data) {
            timetableContainer.empty().html(data);
            buslineSelector.val(busline_id);
        });
    };
    var navigateToSelectedBusline = function() {
        var selected = buslineSelector.val();
        if (selected != 'none') {
            history.pushState('', '', '/#' + selected);
            $(window).trigger('hashchange');
        }
    }

    $(document).ajaxStart(function() { loadingMsg.show(); });
    $(document).ajaxStop(function() { loadingMsg.hide(); });

    buslineSelector.change(navigateToSelectedBusline);
    $(window).on('hashchange', updateTimetable)
    updateTimetable();
});
