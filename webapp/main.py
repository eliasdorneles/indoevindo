from flask import Flask, request
from flask import render_template
from model import Repo

app = Flask(__name__)
app.config['DEBUG'] = True

@app.route('/')
def index():
    repo = Repo()
    return render_template('index.html',
                           companies=repo.get_companies(),
                           buslines=repo.get_buslines())

@app.route('/timetable')
@app.route('/timetable/<busline_id>')
def show_busline(busline_id=None):
    if not busline_id:
        busline_id = request.args.get('busline_id')
    repo = Repo()
    busline = repo.get_busline_by_id(busline_id)
    timetable = repo.get_timetable(busline).by_day_type()
    company = repo.get_company_by_id(busline.company)
    return render_template('timetable.html',
                           busline=busline,
                           timetable=timetable,
                           company=company)
