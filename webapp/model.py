#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import namedtuple, defaultdict
import os
import json
from itertools import groupby


DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static', 'data')
BusCompany = namedtuple('BusCompany', 'id name website enabled')


class BusTimes(object):
    def __init__(self, terminal, company, bus_line, day_type, times, extra=''):
        self.terminal = terminal
        self.company = company
        self.bus_line = bus_line
        self.day_type = day_type
        self.times = times
        self.extra = extra


class BusLine(object):
    def __init__(self, name, company, ext_link, ext_id):
        self.name = name
        self.company = company
        self.ext_link = ext_link
        self.ext_id = ext_id

    def get_key(self):
        return '%s:%s' % (self.company, self.ext_id)


def _agg_by_key(data, key, reverse=False):
    data = sorted(data, key=key, reverse=reverse)
    return {group: list(items) for group, items in groupby(data, key=key)}


def _agg_by_day_type(times):
    return _agg_by_key(times, lambda t: t.day_type, reverse=True)


def _agg_by_terminal(times):
    return _agg_by_key(times, lambda t: t.terminal)


class BusTimetable:
    def __init__(self, times):
        self.times = times

    def __len__(self):
        return len(self.times)

    def __getitem__(self, key):
        return self.times[key]

    def __setitem__(self, key, val):
        self.times[key] = val

    def by_day_type(self):
        return dict((day_type, _agg_by_terminal(items))
                    for day_type, items in
                    _agg_by_day_type(self.times).iteritems())

    def by_terminal(self):
        return dict((terminal, _agg_by_day_type(items))
                    for terminal, items in
                    _agg_by_terminal(self.times).iteritems())


def load_json(jsonfile):
    with open(jsonfile) as f:
        return json.load(f)


def load_buslines_for_company(id_company):
    return load_json('%s/%s/buslines.json' % (DATA_DIR, id_company))


def load_timetable_for_company(id_company):
    return load_json('%s/%s/timetable.json' % (DATA_DIR, id_company))


def _load_companies():
    return [
        BusCompany('canasvieiras', 'Canasvieiras', 'http://www.canasvieirastc.com.br', True),
        BusCompany('estrela', 'Estrela', 'http://www.tcestrela.com.br', False),
        BusCompany('jotur', 'JOTUR', 'http://www.jotur.com.br', True),
        BusCompany('transol', 'Transol', 'http://www.transoltc.com.br', True),
    ]


def get_only_enabled_companies():
    return filter(lambda c: c.enabled, _load_companies())


_buslines = None
_timetables = []


def _load_lines():
    global _buslines
    if not _buslines:
        _buslines = [BusLine(**d)
                     for company in get_only_enabled_companies()
                     for d in load_buslines_for_company(company.id)]
    return _buslines


def _load_bus_times():
    global _timetables
    if not _timetables:
        _timetables = [BusTimes(**d)
                       for company in get_only_enabled_companies()
                       for d in load_timetable_for_company(company.id)]
    return _timetables


class Repo:
    """Repository abstracting obtention of data"""
    def __init__(self):
        self.companies = dict([(c.id, c) for c in get_only_enabled_companies()])
        self.lines = dict((l.get_key(), l) for l in _load_lines())
        self.timetables = self.build_timetables()

    def build_timetables(self):
        timetables = defaultdict(list)
        create_key = lambda time: '%s:%s' % (time.company, time.bus_line)
        for it in _load_bus_times():
            key = create_key(it)
            timetables[key].append(it)
        return dict((k, BusTimetable(v))
                    for k, v in timetables.iteritems())

    def get_buslines(self):
        return self.lines.values()

    def get_companies(self):
        return self.companies.values()

    def get_company_by_id(self, company_id):
        return self.companies.get(company_id)

    def get_busline_by_id(self, busline_id):
        return self.lines.get(busline_id)

    def get_timetable(self, busline):
        return self.timetables.get(busline.get_key())
