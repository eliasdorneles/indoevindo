from scrapy.item import Item, Field

class BusLine(Item):
    name = Field()
    url_name = Field()
    ext_id = Field()
    company = Field()
    ext_link = Field()

class BusLineTimetable(Item):
    bus_line = Field()
    company = Field()
    terminal = Field()
    times = Field()
    day_type = Field()
    extra = Field()
