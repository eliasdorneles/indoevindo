# Scrapy settings for bus_schedules project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'bus_schedules'

SPIDER_MODULES = ['bus_schedules.spiders']
NEWSPIDER_MODULE = 'bus_schedules.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'bus_schedules (+http://www.yourdomain.com)'
