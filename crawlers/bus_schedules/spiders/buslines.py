#!/usr/bin/env python
# -*- coding: utf-8 -*-

from scrapy.spider import Spider
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, Compose
from bus_schedules.items import BusLine
from urlparse import urljoin


def build_compose_url(response_url):
    return Compose(
        TakeFirst(),
        lambda x: urljoin(response_url, x)
    )


class BusLineLoader(ItemLoader):
    default_output_processor = TakeFirst()


class TransolBusLinesSpider(Spider):
    name = "transolBusLines"
    allowed_domains = ["transoltc.com.br"]
    start_urls = ["http://www.transoltc.com.br/principal.php"]

    def parse(self, response):
        for it in response.css('select[name=s_id_linha] option'):
            if not it.xpath('@value'):
                continue

            l = BusLineLoader(BusLine(), it)
            l.add_xpath('ext_id', '@value')
            l.add_xpath('name', 'text()', re=u'(?:» )?(.+)')
            l.add_value('company', 'transol')
            # TODO: check if it is possible to get a direct link
            l.add_value('ext_link', 'http://www.transoltc.com.br')
            yield l.load_item()


class CanasvieirasBusLinesSpider(Spider):
    name = "canasvieirasBusLines"
    allowed_domains = ["canasvieirastc.com.br"]
    start_urls = ["http://www.canasvieirastc.com.br/pt/horarios.html"]

    def parse(self, response):
        for it in response.css('section#pagina ul.list li a'):
            l = BusLineLoader(BusLine(), it)
            l.add_xpath('ext_id', '@href', re=r'/pt/horarios/((d-)?[0-9]+)-.*')
            l.add_xpath('name', 'text()')
            l.add_value('company', 'canasvieiras')
            l.add_xpath('ext_link', '@href', build_compose_url(response.url))
            yield l.load_item()


class EstrelaBusLinesSpider(Spider):
    name = "estrelaBusLines"
    allowed_domains = ["tcestrela.com.br"]
    start_urls = [
        "http://www.tcestrela.com.br/ws1/linhas/entradas/municipal.htm",
        "http://www.tcestrela.com.br/ws1/linhas/entradas/intermunicipal.htm",
        "http://www.tcestrela.com.br/ws1/linhas/entradas/interbairros.htm",
    ]

    def parse(self, response):
        for it in response.xpath('//table/tr/td/strong/a'):
            l = BusLineLoader(BusLine(), it)
            l.add_xpath('ext_id', '@href', re=r'.*/([^/]+).html?')
            l.add_xpath('name', 'text()')
            l.add_value('company', 'estrela')
            l.add_xpath('ext_link', '@href', build_compose_url(response.url))
            yield l.load_item()


class JoturBusLinesSpider(Spider):
    name = "joturBusLines"
    allowed_domains = ["jotur.com.br"]
    start_urls = ["http://www.jotur.com.br/index.php?cmd=horarios"]

    def parse(self, response):
        for it in response.css('a.table-linha-content'):
            l = BusLineLoader(BusLine(), it)
            l.add_xpath('ext_id', '@href', Compose(
                TakeFirst(), lambda x: x.split('=')[-1]))
            l.add_xpath('name', '@title')
            l.add_value('company', 'jotur')
            l.add_xpath('ext_link', '@href', build_compose_url(response.url))
            yield l.load_item()
