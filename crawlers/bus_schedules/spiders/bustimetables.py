from scrapy.spider import Spider
from scrapy.contrib.loader import ItemLoader
from scrapy.contrib.loader.processor import TakeFirst, Compose, Identity
from scrapy.http import Request, FormRequest
from bus_schedules.items import BusLineTimetable
import json


def load_json(src):
    with open(src) as f:
        return json.load(f)


class BusTimetableLoader(ItemLoader):
    default_item_class = BusLineTimetable
    default_output_processor = TakeFirst()
    times_out = Identity()


class GenericBusTimetable(Spider):
    def __init__(self, linesfile):
        self.lines = load_json(linesfile)

    def start_requests(self):
        for line in self.lines:
            yield Request(url=line['ext_link'], meta=line)


class TransolBusTimetable(GenericBusTimetable):
    name = 'transolBusTimetable'

    def start_requests(self):
        url = 'http://www.transoltc.com.br/secoes/horario_impressao.php'

        for line in self.lines:
            yield FormRequest(
                url,
                formdata=dict(
                    s_id_linha=line.get('ext_id'),
                    s_periodo_escolhido='1',
                ),
                meta=line,
            )

    def parse(self, response):
        line_info = response.meta
        self.log('Going to scrape timetable for %s on URL %s' % (line_info['ext_id'],
                                                                 response.url))
        get_non_empty = lambda sel: ' '.join(
            sel.xpath(".//text()[normalize-space(.)]").extract())

        for sel in response.xpath("//table[./tr[2][re:test(., 'Sa.da.+')]]"):
            day_type = sel.xpath("normalize-space(./tr[1])").extract()[0]
            if day_type == 'Domigos':
                day_type = 'Domingos'
            for i, terminal_sel in enumerate(sel.xpath("./tr[2]/td"), 1):
                terminal = get_non_empty(terminal_sel)
                times_xpath = "./tr[3]/td[%d]/table//td[not(./img)]" % i
                times = sorted([get_non_empty(s) for s in sel.xpath(times_xpath)])
                yield BusLineTimetable(
                    times=times,
                    day_type=day_type,
                    terminal=terminal,
                    bus_line=line_info['ext_id'],
                    company=line_info['company'],
                )


class CanasvieirasBusTimetable(GenericBusTimetable):
    name = "canasvieirasBusTimetable"

    def parse(self, response):
        line_info = response.meta
        split = lambda s: s.split('-')
        trim = lambda s: s.strip()
        takeSecond = lambda l: l[1]

        for timelist in response.css('.horarios'):
            l = BusTimetableLoader(selector=timelist)
            l.add_css('day_type', 'b::text', Compose(
                TakeFirst(), split, TakeFirst(), trim))
            l.add_css('terminal', 'b::text', Compose(
                TakeFirst(), split, takeSecond, trim))
            l.add_css('times', 'ul li::text')
            l.add_value('bus_line', line_info['ext_id'])
            l.add_value('company', line_info['company'])
            yield l.load_item()


def extract_first(sel):
    return TakeFirst()(sel.extract())


class JoturBusTimetable(GenericBusTimetable):
    name = 'joturBusTimetable'

    def parse(self, response):
        line_info = response.meta
        for terminal_table in response.css('.junta3'):
            terminal = extract_first(terminal_table.css('h1::text'))
            for lines_table in terminal_table.css('div.fundo table'):
                day_type = extract_first(lines_table.css('.dia::text'))
                for linetime in lines_table.css('tbody tr'):
                    l = BusTimetableLoader(selector=linetime)
                    l.add_css('times', '.saida::text')
                    l.add_value('extra', self.get_extra(linetime))
                    l.add_value('terminal', terminal)
                    l.add_value('day_type', day_type)
                    l.add_value('bus_line', line_info['ext_id'])
                    l.add_value('company', line_info['company'])
                    yield l.load_item()

    def get_extra(self, linetime):
        """Get additional bus line information, Jotur puts these inline"""
        number = extract_first(linetime.css('.linha::text'))
        via = extract_first(linetime.css('.via::text'))
        return number + ('-%s' % via if via else '')
