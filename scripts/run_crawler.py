#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Helper script to crawl and store data in JSON files

from __future__ import print_function
import os
import subprocess
import shutil

PROJ_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DATA_DIR = os.path.join(PROJ_DIR, 'webapp', 'static', 'data')
CRAWLER_DIR = os.path.join(PROJ_DIR, 'crawlers')


def crawl_busline(company, outfile):
    spider = company + 'BusLines'
    args = ['-o', outfile, '-t', 'json']
    print(subprocess.check_output(['scrapy', 'crawl', spider] + args))


def crawl_bustimetables(company, infile, outfile):
    spider = company + 'BusTimetable'
    args = ['-a', 'linesfile=' + infile, '-o', outfile, '-t', 'json']
    print(subprocess.check_output(['scrapy', 'crawl', spider] + args))


def crawl_and_store_data_for(company, target_dir):
    linesfile = os.path.join(target_dir, 'buslines.json')
    if not os.path.exists(linesfile):
        crawl_busline(company, linesfile)

    timetablefile = os.path.join(target_dir, 'timetable.json')
    if not os.path.exists(timetablefile):
        crawl_bustimetables(company, linesfile, timetablefile)


def clean_data_dirs(data_dir, companies):
    for company in companies:
        remove_dir = os.path.join(data_dir, company)
        shutil.rmtree(remove_dir, ignore_errors=True)


def run(args):
    os.chdir(CRAWLER_DIR)

    if args.clean_data:
        clean_data_dirs(args.data_dir, args.company)

    for company in args.company:
        target_dir = os.path.join(args.data_dir, company)

        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

        crawl_and_store_data_for(company, target_dir)


if '__main__' == __name__:
    import argparse
    parser = argparse.ArgumentParser(
        description="Helper script to crawl and store data in JSON files")
    parser.add_argument('--data-dir', default=DATA_DIR,
                        help='Directory where to store the JSON files')
    parser.add_argument('--clean-data', action='store_true',
                        help='Delete the data directory before getting data')

    parser.add_argument('company', nargs='+',
                        # for now, the crawlers only work for these two companies:
                        choices=set(['canasvieiras', 'jotur', 'transol']),
                        help='Website company to crawl')

    args = parser.parse_args()
    run(args)
