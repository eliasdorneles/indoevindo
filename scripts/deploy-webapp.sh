#!/bin/bash

abort() {
    echo "$*"; exit 1;
}

usage() {
    echo "Usage: $(basename $0) [--tests-only|--skip-tests]"
    echo
    echo "       Run tests requires py.test tool installed"
    exit 1
}

realpath(){
    if echo "$1" | grep -q '^/'
    then
        echo "$1"
    else
        echo $(cd $(dirname "$1") && pwd)/$(basename "$1")
    fi
}

proj_dir=$(dirname $(dirname $(realpath $0)))
webapp_dir=$proj_dir/webapp

deploy_app=true
run_tests=true
while [ "${1#-}" != "$1" ]; do
    case "$1" in
        -h) usage;;
        --tests-only) deploy_app=false;;
        --skip-tests) run_tests=false;;
        *) usage;;
    esac
    shift
done

# avoids a deploy if any test does not pass
set -e

cd "$webapp_dir" || abort "Could not find webapp dir: $webapp_dir"

$run_tests && PYTHONPATH=. py.test --tb=short --capture=no --ignore=lib

$deploy_app && appcfg.py update .
